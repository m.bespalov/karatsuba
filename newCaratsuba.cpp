#include <cstring>
#include<iostream>
#include<time.h>
#include<Windows.h>

#define BASE 10 //������� ���������
#define MIN_LENGTH_FOR_KARATSUBA 4 //����� ������ ���������� ������������ ����������
typedef int digit; //���� ������ ��� �������� �����
typedef unsigned long int size_length; //��� ��� ����� �����

using namespace std;

struct long_value { //��� ��� ������� �����
    digit* values; //������ � ������� ����� ����������� � �������� �������
    size_length length; //����� �����
};

long_value sum(long_value a, long_value b) {
    /* ������� ��� ������������ ���� ������� �����. ���� ����������� ����� ������ ������
    * �� ����� ������� ��������� � �������� ������� ���������. ���������� �����
    * ����������������� �����.
    */
    long_value s;
    s.length = a.length + 1;
    s.values = new digit[s.length];

    s.values[a.length - 1] = a.values[a.length - 1];
    s.values[a.length] = 0;
    for (size_length i = 0; i < b.length; ++i)
        s.values[i] = a.values[i] + b.values[i];
    return s;
}

long_value& sub(long_value& a, long_value b) {
    /*������� ��� ��������� ������ �������� ����� �� �������. �������� ���������� �������
    * �����. ���������� ������ �� ������ �����. ��������� �� ������������.
    */
    for (size_length i = 0; i < b.length; ++i)
        a.values[i] -= b.values[i];
    return a;
}

void normalize(long_value l) {
    /*������������ ����� - ���������� ������� ������� � ������������ � �������� ���������.
    */
    for (size_length i = 0; i < l.length - 1; ++i) {
        if (l.values[i] >= BASE) { //���� ����� ������ �������������, �� ���������������� �������
            digit carryover = l.values[i] / BASE;
            l.values[i + 1] += carryover;
            l.values[i] -= carryover * BASE;
        }
        else if (l.values[i] < 0) { //���� ������ - ����
            digit carryover = (l.values[i] + 1) / BASE - 1;
            l.values[i + 1] += carryover;
            l.values[i] -= carryover * BASE;
        }
    }
}

long_value karatsuba(long_value a, long_value b) {
    long_value product; //�������������� ������������
    product.length = a.length + b.length;
    product.values = new digit[product.length];

    if (a.length < MIN_LENGTH_FOR_KARATSUBA) { //���� ����� ������ �� ��������� ������� ���������
        memset(product.values, 0, sizeof(digit) * product.length);
        for (size_length i = 0; i < a.length; ++i)
            for (size_length j = 0; j < b.length; ++j) {
                product.values[i + j] += a.values[i] * b.values[j];
                /*� ������ ��������� MIN_LENGTH_FOR_KARATSUBA ��� BASE ����������������� ���������
                * ������ � ��������� �����. �������� ��� ���������� ������������ ��������.
                * �������� ��� ���������� ������� ��������� ����� 100, ��������, ��� ����������������
                * ������� 1 ����� ���� ������, 200 - ������� 2 ����� ���� �������, 5000 - 5 ����� ���.
                * if (product.values[i + j] >= 100){
                *   product.values[i + j] -= 100;
                *   product.values[i + j + 2] += 1;
                * }
                */
            }
    }
    else { //��������� ������� ��������
        long_value a_part1; //������� ����� ����� a
        a_part1.values = a.values;
        a_part1.length = (a.length + 1) / 2;

        long_value a_part2; //������� ����� ����� a
        a_part2.values = a.values + a_part1.length;
        a_part2.length = a.length / 2;

        long_value b_part1; //������� ����� ����� b
        b_part1.values = b.values;
        b_part1.length = (b.length + 1) / 2;

        long_value b_part2; //������� ����� ����� b
        b_part2.values = b.values + b_part1.length;
        b_part2.length = b.length / 2;

        long_value sum_of_a_parts = sum(a_part1, a_part2); //c���� ������ ����� a
        normalize(sum_of_a_parts);
        long_value sum_of_b_parts = sum(b_part1, b_part2); //c���� ������ ����� b
        normalize(sum_of_b_parts);
        long_value product_of_sums_of_parts = karatsuba(sum_of_a_parts, sum_of_b_parts);
        // ������������ ���� ������

        long_value product_of_first_parts = karatsuba(a_part1, b_part1); //������� ����
        long_value product_of_second_parts = karatsuba(a_part2, b_part2); //������� ����
        long_value sum_of_middle_terms = sub(sub(product_of_sums_of_parts, product_of_first_parts), product_of_second_parts);
        //���������� ����� ������� ������

        /*
        * ������������ ����������
        */
        memcpy(product.values, product_of_first_parts.values,
            product_of_first_parts.length * sizeof(digit));
        memcpy(product.values + product_of_first_parts.length,
            product_of_second_parts.values, product_of_second_parts.length
            * sizeof(digit));
        for (size_length i = 0; i < sum_of_middle_terms.length; ++i)
            product.values[a_part1.length + i] += sum_of_middle_terms.values[i];

        /*
        * ��������
        */
        delete[] sum_of_a_parts.values;
        delete[] sum_of_b_parts.values;
        delete[] product_of_sums_of_parts.values;
        delete[] product_of_first_parts.values;
        delete[] product_of_second_parts.values;
    }

    normalize(product); //�������� ������������ �����

    return product;
}

const int MAX_SIZE = 10;

long_value digits1, digits2;


void init1(int razmer) {
    digits1.length = razmer;
    digits1.values = new digit[digits1.length];
    for (int i = 0; i < razmer; i++) {
        digits1.values[i] = 0;
    }
}
void init2(int razmer) {
    digits2.length = razmer;
    digits2.values = new digit[digits2.length];
    for (int i = 0; i < razmer; i++) {
        digits2.values[i] = 0;
    }
}

void generate(long_value a) {
    a.values[a.length - 1] = (rand() % 9) + 1;
    cout << a.values[a.length - 1];
    for (int i = 0; i < a.length - 1; i++) {
        a.values[i] = rand() % 10;
    }    
    for (int i = a.length - 2; i >= 0; i--)
        cout << a.values[i];
}

void PrintResult(int number1_current_size, int number2_current_size) {
    
    init1(number1_current_size);
    init2(number2_current_size);

    generate(digits1);
    cout << "\n";
    generate(digits2);

    long_value result;
    result.length = karatsuba(digits1, digits2).length;
    result.values = new digit[result.length];
    for (int i = 0; i < result.length; i++) {
        result.values[i] = karatsuba(digits1, digits2).values[i];
    }

    cout << "\nResult(" << number1_current_size << ")*(" << number2_current_size << ") = ";
    if (result.values[result.length - 1] != 0) cout << result.values[result.length - 1];
    for (int q = result.length - 2; q >= 0; q--) {
        cout << result.values[q];
    }
    cout << "\n\n";
}

void main() {
    srand(time(NULL));

    //PrintResult(2, 3);

    for (int number1_current_size = 2; number1_current_size < MAX_SIZE; number1_current_size++) {
        for (int number2_current_size = 2; number2_current_size < MAX_SIZE; number2_current_size++) {
            PrintResult(number1_current_size, number2_current_size);
        }
    }

}